package domain

import (
	"time"
)

type Task struct {
	TaskId      int        `json:"task_id"`
	WorkspaceId int        `json:"workspace_id"`
	OwnerId     int        `json:"owner_id"`
	Status      string     `json:"status"`
	CreatedAt   *time.Time `json:"created_at"`
	Name        string     `json:"name"`
	Description string     `json:"description"`
	Deadline    *time.Time `json:"deadline"`
}

type TaskFilter struct {
	WorkspaceId   int        `json:"workspace_id"`
	Status        string     `json:"status"`
	CreatedAtFrom *time.Time `json:"created_at_from"`
	CreatedAtTo   *time.Time `json:"created_at_to"`
	Name          string     `json:"name"`
	Description   string     `json:"description"`
	DeadlineFrom  *time.Time `json:"deadline_from"`
	DeadlineTo    *time.Time `json:"deadline_to"`
	Page          int        `json:"page"`
	Size          int        `json:"size"`
}

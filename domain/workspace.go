package domain

type Workspace struct {
	WorkspaceId int    `json:"workspace_id"`
	OwnerId     int    `json:"owner_id"`
	Name        string `json:"name"`
}

type WorkspaceFilter struct {
	Page int `json:"page"`
	Size int `json:"size"`
}

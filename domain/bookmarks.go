package domain

type Bookmark struct {
	BookmarkId  int    `json:"bookmark_id"`
	OwnerId     int    `json:"owner_id"`
	Name        string `json:"name"`
	Description string `json:"description"`
}

type BookmarkFilter struct {
	Name        string `json:"name"`
	Description string `json:"description"`
	Page        int    `json:"page"`
	Size        int    `json:"size"`
}

package domain

import "time"

type Subtask struct {
	SubtaskId   int        `json:"subtask_id"`
	TaskId      int        `json:"task_id"`
	OwnerId     int        `json:"owner_id"`
	CreatedAt   *time.Time `json:"created_at"`
	Name        string     `json:"name"`
	Description string     `json:"description"`
	Status      string     `json:"status"`
}

type SubtaskFilter struct {
	TaskId        int        `json:"task_id"`
	CreatedAtFrom *time.Time `json:"created_at_from"`
	CreatedAtTo   *time.Time `json:"created_at_to"`
	Name          string     `json:"name"`
	Description   string     `json:"description"`
	Status        string     `json:"status"`
	Page          int        `json:"page"`
	Size          int        `json:"size"`
}

package domain

import (
	"github.com/dgrijalva/jwt-go"
)

const (
	REGISTER = "register"
	AUTH     = "auth"
	REFRESH  = "refresh"
	CREATE   = "create"
	READ     = "read"
	UPDATE   = "update"
	DELETE   = "delete"

	USER               = "user_id"
	JWTTokenContextKey = "token_key"
)

type User struct {
	Login    string
	Password string
	Id       int
}

type Claims struct {
	Id    int    `json:"id"`
	Login string `json:"login"`
	jwt.StandardClaims
}

type Token struct {
	Token string
}

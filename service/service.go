package service

import (
	"context"
	"database/sql"
	"errors"
	log "github.com/sirupsen/logrus"

	"github.com/Abunyawa/cinderella/domain"
	"github.com/Abunyawa/cinderella/store"
)

// Methods declared here
type Service interface {
	ExampleServiceMethod(name string) (string, error)
	AddUser(user *domain.User) error
	AuthUser(user *domain.User) (string, error)
	RefreshToken(token *domain.Token) (string, error)

	CreateWorkspace(ctx context.Context, ws domain.Workspace) error
	ReadWorkspace(ctx context.Context, id int) (ws domain.Workspace, err error)
	UpdateWorkspace(ctx context.Context, ws domain.Workspace) (domain.Workspace, error)
	DeleteWorkspace(ctx context.Context, id int) error
	ReadWorkspaces(ctx context.Context, filter domain.WorkspaceFilter) ([]domain.Workspace, error)

	CreateBookmark(ctx context.Context, b domain.Bookmark) error
	ReadBookmark(ctx context.Context, id int) (ws domain.Bookmark, err error)
	UpdateBookmark(ctx context.Context, b domain.Bookmark) (domain.Bookmark, error)
	DeleteBookmark(ctx context.Context, id int) error
	ReadBookmarks(ctx context.Context, filter domain.BookmarkFilter) ([]domain.Bookmark, error)

	CreateTask(ctx context.Context, t domain.Task) error
	ReadTask(ctx context.Context, id int) (ws domain.Task, err error)
	UpdateTask(ctx context.Context, t domain.Task) (domain.Task, error)
	DeleteTask(ctx context.Context, id int) error
	ReadTasks(ctx context.Context, filter domain.TaskFilter) ([]domain.Task, error)

	CreateSubtask(ctx context.Context, st domain.Subtask) error
	ReadSubtask(ctx context.Context, id int) (ws domain.Subtask, err error)
	UpdateSubtask(ctx context.Context, st domain.Subtask) (domain.Subtask, error)
	DeleteSubtask(ctx context.Context, id int) error
	ReadSubtasks(ctx context.Context, filter domain.SubtaskFilter) ([]domain.Subtask, error)
}

type service struct {
	Store   store.Store
	SignKey []byte
}

func NewService(db *sql.DB, key string) Service {

	return &service{
		Store:   store.NewStore(db),
		SignKey: []byte(key),
	}
}

func getUserId(ctx context.Context, method string) (int, error) {
	userId, ok := ctx.Value(domain.USER).(int)
	if !ok {
		log.WithFields(log.Fields{
			"method":  method,
			"message": "user_id not provided",
		}).Error("user_id not provided")
		return 0, errors.New("user id not provided")
	}

	return userId, nil
}

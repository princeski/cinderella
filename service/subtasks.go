package service

import (
	"context"
	"errors"
	"github.com/Abunyawa/cinderella/domain"
	log "github.com/sirupsen/logrus"
)

func (s *service) CreateSubtask(ctx context.Context, st domain.Subtask) error {
	userId, err := getUserId(ctx, domain.CREATE)
	if err != nil {
		return err
	}
	st.OwnerId = userId
	st.Status = "TO DO"
	err = s.Store.CreateSubtask(st)

	return err
}

func (s *service) ReadSubtask(ctx context.Context, id int) (st domain.Subtask, err error) {
	return s.Store.ReadSubtask(id)
}

func (s *service) UpdateSubtask(ctx context.Context, st domain.Subtask) (domain.Subtask, error) {
	userId, err := getUserId(ctx, domain.UPDATE)
	if err != nil {
		return st, err
	}

	if st.OwnerId != userId {
		log.WithFields(log.Fields{
			"method":  domain.UPDATE,
			"message": "forbidden",
		}).Error("owner_id != user_id")
		return st, errors.New("forbidden")
	}
	return s.Store.UpdateSubtask(st)
}

func (s *service) DeleteSubtask(ctx context.Context, id int) error {
	userId, err := getUserId(ctx, domain.DELETE)
	if err != nil {
		return err
	}

	return s.Store.DeleteSubtask(id, userId)
}

func (s *service) ReadSubtasks(ctx context.Context, filter domain.SubtaskFilter) ([]domain.Subtask, error) {
	userId, err := getUserId(ctx, domain.READ)
	if err != nil {
		return nil, err
	}

	return s.Store.ReadSubtasks(filter, userId)
}

package service

import (
	"context"
	"errors"
	"github.com/Abunyawa/cinderella/domain"
	log "github.com/sirupsen/logrus"
)

func (s *service) CreateWorkspace(ctx context.Context, ws domain.Workspace) error {
	userId, err := getUserId(ctx, domain.CREATE)
	if err != nil {
		return err
	}
	ws.OwnerId = userId
	err = s.Store.CreateWorkspace(ws)

	return err
}

func (s *service) ReadWorkspace(ctx context.Context, id int) (ws domain.Workspace, err error) {
	return s.Store.ReadWorkspace(id)
}

func (s *service) UpdateWorkspace(ctx context.Context, ws domain.Workspace) (domain.Workspace, error) {
	userId, err := getUserId(ctx, domain.UPDATE)
	if err != nil {
		return ws, err
	}

	if ws.OwnerId != userId {
		log.WithFields(log.Fields{
			"method":  domain.UPDATE,
			"message": "forbidden",
		}).Error("owner_id != user_id")
		return ws, errors.New("forbidden")
	}
	return s.Store.UpdateWorkspace(ws)
}

func (s *service) DeleteWorkspace(ctx context.Context, id int) error {
	userId, err := getUserId(ctx, domain.DELETE)
	if err != nil {
		return err
	}

	return s.Store.DeleteWorkspace(id, userId)
}

func (s *service) ReadWorkspaces(ctx context.Context, filter domain.WorkspaceFilter) ([]domain.Workspace, error) {
	userId, err := getUserId(ctx, domain.READ)
	if err != nil {
		return nil, err
	}

	return s.Store.ReadWorkspaces(filter, userId)
}

package service

import (
	"context"
	"errors"
	"github.com/Abunyawa/cinderella/domain"
	log "github.com/sirupsen/logrus"
)

func (s *service) CreateBookmark(ctx context.Context, b domain.Bookmark) error {
	userId, err := getUserId(ctx, domain.CREATE)
	if err != nil {
		return err
	}
	b.OwnerId = userId
	err = s.Store.CreateBookmark(b)

	return err
}

func (s *service) ReadBookmark(ctx context.Context, id int) (b domain.Bookmark, err error) {
	return s.Store.ReadBookmark(id)
}

func (s *service) UpdateBookmark(ctx context.Context, b domain.Bookmark) (domain.Bookmark, error) {
	userId, err := getUserId(ctx, domain.UPDATE)
	if err != nil {
		return b, err
	}

	if b.OwnerId != userId {
		log.WithFields(log.Fields{
			"method":  domain.UPDATE,
			"message": "forbidden",
		}).Error("owner_id != user_id")
		return b, errors.New("forbidden")
	}
	return s.Store.UpdateBookmark(b)
}

func (s *service) DeleteBookmark(ctx context.Context, id int) error {
	userId, err := getUserId(ctx, domain.DELETE)
	if err != nil {
		return err
	}

	return s.Store.DeleteBookmark(id, userId)
}

func (s *service) ReadBookmarks(ctx context.Context, filter domain.BookmarkFilter) ([]domain.Bookmark, error) {
	userId, err := getUserId(ctx, domain.READ)
	if err != nil {
		return nil, err
	}

	return s.Store.ReadBookmarks(filter, userId)
}

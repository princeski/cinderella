package service

import (
	"context"
	"errors"
	"github.com/Abunyawa/cinderella/domain"
	log "github.com/sirupsen/logrus"
)

func (s *service) CreateTask(ctx context.Context, t domain.Task) error {
	userId, err := getUserId(ctx, domain.CREATE)
	if err != nil {
		return err
	}
	t.OwnerId = userId
	t.Status = "TO DO"
	err = s.Store.CreateTask(t)

	return err
}

func (s *service) ReadTask(ctx context.Context, id int) (t domain.Task, err error) {
	return s.Store.ReadTask(id)
}

func (s *service) UpdateTask(ctx context.Context, t domain.Task) (domain.Task, error) {
	userId, err := getUserId(ctx, domain.UPDATE)
	if err != nil {
		return t, err
	}

	if t.OwnerId != userId {
		log.WithFields(log.Fields{
			"method":  domain.UPDATE,
			"message": "forbidden",
		}).Error("owner_id != user_id")
		return t, errors.New("forbidden")
	}
	return s.Store.UpdateTask(t)
}

func (s *service) DeleteTask(ctx context.Context, id int) error {
	userId, err := getUserId(ctx, domain.DELETE)
	if err != nil {
		return err
	}

	return s.Store.DeleteTask(id, userId)
}

func (s *service) ReadTasks(ctx context.Context, filter domain.TaskFilter) ([]domain.Task, error) {
	userId, err := getUserId(ctx, domain.READ)
	if err != nil {
		return nil, err
	}

	return s.Store.ReadTasks(filter, userId)
}

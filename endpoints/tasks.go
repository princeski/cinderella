package endpoints

import (
	"context"
	"github.com/Abunyawa/cinderella/domain"
	"github.com/Abunyawa/cinderella/service"
	"github.com/go-kit/kit/endpoint"
	"time"
)

type CreateTaskRequest struct {
	WorkspaceId int        `json:"workspace_id"`
	Name        string     `json:"name"`
	Description string     `json:"description"`
	Deadline    *time.Time `json:"deadline"`
}

type CreateTaskResponse struct {
	Result string `json:"result"`
}

func MakeCreateTaskEndpoint(s service.Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (response interface{}, err error) {
		req := request.(*CreateTaskRequest)

		err = s.CreateTask(
			ctx,
			domain.Task{
				WorkspaceId: req.WorkspaceId,
				Name:        req.Name,
				Description: req.Description,
				Deadline:    req.Deadline,
			},
		)

		if err != nil {
			return nil, err
		}

		return CreateTaskResponse{
			Result: "Created successfully",
		}, nil
	}
}

type ReadTaskRequest struct {
	Id int `json:"id"`
}

type ReadTaskResponse struct {
	Task domain.Task `json:"task"`
}

func MakeReadTaskEndpoint(s service.Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (response interface{}, err error) {
		req := request.(*ReadTaskRequest)

		t, err := s.ReadTask(
			ctx,
			req.Id,
		)

		if err != nil {
			return nil, err
		}

		return ReadTaskResponse{
			Task: t,
		}, nil
	}
}

type UpdateTaskRequest struct {
	// swagger:ignore
	TaskId      int        `json:"task_id"`
	WorkspaceId int        `json:"workspace_id"`
	OwnerId     int        `json:"owner_id"`
	Status      string     `json:"status"`
	CreatedAt   *time.Time `json:"created_at"`
	Name        string     `json:"name"`
	Description string     `json:"description"`
	Deadline    *time.Time `json:"deadline"`
}

type UpdateTaskResponse struct {
	Task domain.Task `json:"task"`
}

func MakeUpdateTaskEndpoint(s service.Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (response interface{}, err error) {
		req := request.(*UpdateTaskRequest)

		t, err := s.UpdateTask(
			ctx,
			domain.Task{
				TaskId:      req.TaskId,
				WorkspaceId: req.WorkspaceId,
				OwnerId:     req.OwnerId,
				Status:      req.Status,
				CreatedAt:   req.CreatedAt,
				Name:        req.Name,
				Description: req.Description,
				Deadline:    req.Deadline,
			},
		)

		if err != nil {
			return nil, err
		}

		return UpdateTaskResponse{
			Task: t,
		}, nil
	}
}

type DeleteTaskRequest struct {
	Id int `json:"id"`
}

type DeleteTaskResponse struct {
	Result string `json:"result"`
}

func MakeDeleteTaskEndpoint(s service.Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (response interface{}, err error) {
		req := request.(*DeleteTaskRequest)

		err = s.DeleteTask(
			ctx,
			req.Id,
		)

		if err != nil {
			return nil, err
		}

		return DeleteTaskResponse{
			Result: "Deleted successfully",
		}, nil
	}
}

type ReadTasksRequest struct {
	Filter domain.TaskFilter `json:"filter"`
}

type ReadTasksResponse struct {
	Tasks []domain.Task `json:"tasks"`
}

func MakeReadTasksEndpoint(s service.Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (response interface{}, err error) {
		req := request.(*ReadTasksRequest)

		t, err := s.ReadTasks(
			ctx,
			req.Filter,
		)

		if err != nil {
			return nil, err
		}

		return ReadTasksResponse{
			Tasks: t,
		}, nil
	}
}

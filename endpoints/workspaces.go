package endpoints

import (
	"context"
	"github.com/Abunyawa/cinderella/domain"
	"github.com/Abunyawa/cinderella/service"
	"github.com/go-kit/kit/endpoint"
)

type CreateWorkspaceRequest struct {
	Name string `json:"name"`
}

type CreateWorkspaceResponse struct {
	Result string `json:"result"`
}

func MakeCreateWorkspaceEndpoint(s service.Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (response interface{}, err error) {
		req := request.(*CreateWorkspaceRequest)

		err = s.CreateWorkspace(
			ctx,
			domain.Workspace{
				Name: req.Name,
			},
		)

		if err != nil {
			return nil, err
		}

		return CreateWorkspaceResponse{
			Result: "Created successfully",
		}, nil
	}
}

type ReadWorkspaceRequest struct {
	Id int `json:"id"`
}

type ReadWorkspaceResponse struct {
	Workspace domain.Workspace `json:"workspace"`
}

func MakeReadWorkspaceEndpoint(s service.Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (response interface{}, err error) {
		req := request.(*ReadWorkspaceRequest)

		ws, err := s.ReadWorkspace(
			ctx,
			req.Id,
		)

		if err != nil {
			return nil, err
		}

		return ReadWorkspaceResponse{
			Workspace: ws,
		}, nil
	}
}

type UpdateWorkspaceRequest struct {
	// swagger:ignore
	WorkspaceId int    `json:"workspace_id"`
	OwnerId     int    `json:"owner_id"`
	Name        string `json:"name"`
}

type UpdateWorkspaceResponse struct {
	Workspace domain.Workspace `json:"workspace"`
}

func MakeUpdateWorkspaceEndpoint(s service.Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (response interface{}, err error) {
		req := request.(*UpdateWorkspaceRequest)

		ws, err := s.UpdateWorkspace(
			ctx,
			domain.Workspace{
				WorkspaceId: req.WorkspaceId,
				OwnerId:     req.OwnerId,
				Name:        req.Name,
			},
		)

		if err != nil {
			return nil, err
		}

		return UpdateWorkspaceResponse{
			Workspace: ws,
		}, nil
	}
}

type DeleteWorkspaceRequest struct {
	Id int `json:"id"`
}

type DeleteWorkspaceResponse struct {
	Result string `json:"result"`
}

func MakeDeleteWorkspaceEndpoint(s service.Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (response interface{}, err error) {
		req := request.(*DeleteWorkspaceRequest)

		err = s.DeleteWorkspace(
			ctx,
			req.Id,
		)

		if err != nil {
			return nil, err
		}

		return DeleteWorkspaceResponse{
			Result: "Deleted successfully",
		}, nil
	}
}

type ReadWorkspacesRequest struct {
	Filter domain.WorkspaceFilter `json:"filter"`
}

type ReadWorkspacesResponse struct {
	Workspaces []domain.Workspace `json:"workspaces"`
}

func MakeReadWorkspacesEndpoint(s service.Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (response interface{}, err error) {
		req := request.(*ReadWorkspacesRequest)

		ws, err := s.ReadWorkspaces(
			ctx,
			req.Filter,
		)

		if err != nil {
			return nil, err
		}

		return ReadWorkspacesResponse{
			Workspaces: ws,
		}, nil
	}
}

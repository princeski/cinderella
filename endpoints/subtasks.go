package endpoints

import (
	"context"
	"github.com/Abunyawa/cinderella/domain"
	"github.com/Abunyawa/cinderella/service"
	"github.com/go-kit/kit/endpoint"
	"time"
)

type CreateSubtaskRequest struct {
	TaskId      int    `json:"task_id"`
	Name        string `json:"name"`
	Description string `json:"description"`
}

type CreateSubtaskResponse struct {
	Result string `json:"result"`
}

func MakeCreateSubtaskEndpoint(s service.Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (response interface{}, err error) {
		req := request.(*CreateSubtaskRequest)

		err = s.CreateSubtask(
			ctx,
			domain.Subtask{
				TaskId:      req.TaskId,
				Name:        req.Name,
				Description: req.Description,
			},
		)

		if err != nil {
			return nil, err
		}

		return CreateSubtaskResponse{
			Result: "Created successfully",
		}, nil
	}
}

type ReadSubtaskRequest struct {
	Id int `json:"id"`
}

type ReadSubtaskResponse struct {
	Subtask domain.Subtask `json:"subtask"`
}

func MakeReadSubtaskEndpoint(s service.Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (response interface{}, err error) {
		req := request.(*ReadSubtaskRequest)

		st, err := s.ReadSubtask(
			ctx,
			req.Id,
		)

		if err != nil {
			return nil, err
		}

		return ReadSubtaskResponse{
			Subtask: st,
		}, nil
	}
}

type UpdateSubtaskRequest struct {
	// swagger:ignore
	SubtaskId   int        `json:"subtask_id"`
	TaskId      int        `json:"task_id"`
	OwnerId     int        `json:"owner_id"`
	CreatedAt   *time.Time `json:"created_at"`
	Name        string     `json:"name"`
	Description string     `json:"description"`
	Status      string     `json:"status"`
}

type UpdateSubtaskResponse struct {
	Subtask domain.Subtask `json:"subtask"`
}

func MakeUpdateSubtaskEndpoint(s service.Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (response interface{}, err error) {
		req := request.(*UpdateSubtaskRequest)

		st, err := s.UpdateSubtask(
			ctx,
			domain.Subtask{
				SubtaskId:   req.SubtaskId,
				TaskId:      req.TaskId,
				OwnerId:     req.OwnerId,
				CreatedAt:   req.CreatedAt,
				Name:        req.Name,
				Description: req.Description,
				Status:      req.Status,
			},
		)

		if err != nil {
			return nil, err
		}

		return UpdateSubtaskResponse{
			Subtask: st,
		}, nil
	}
}

type DeleteSubtaskRequest struct {
	Id int `json:"id"`
}

type DeleteSubtaskResponse struct {
	Result string `json:"result"`
}

func MakeDeleteSubtaskEndpoint(s service.Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (response interface{}, err error) {
		req := request.(*DeleteSubtaskRequest)

		err = s.DeleteSubtask(
			ctx,
			req.Id,
		)

		if err != nil {
			return nil, err
		}

		return DeleteSubtaskResponse{
			Result: "Deleted successfully",
		}, nil
	}
}

type ReadSubtasksRequest struct {
	Filter domain.SubtaskFilter `json:"filter"`
}

type ReadSubtasksResponse struct {
	Subtasks []domain.Subtask `json:"subtasks"`
}

func MakeReadSubtasksEndpoint(s service.Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (response interface{}, err error) {
		req := request.(*ReadSubtasksRequest)

		st, err := s.ReadSubtasks(
			ctx,
			req.Filter,
		)

		if err != nil {
			return nil, err
		}

		return ReadSubtasksResponse{
			Subtasks: st,
		}, nil
	}
}

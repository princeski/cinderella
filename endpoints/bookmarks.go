package endpoints

import (
	"context"
	"github.com/Abunyawa/cinderella/domain"
	"github.com/Abunyawa/cinderella/service"
	"github.com/go-kit/kit/endpoint"
)

type CreateBookmarkRequest struct {
	Name        string `json:"name"`
	Description string `json:"description"`
}

type CreateBookmarkResponse struct {
	Result string `json:"result"`
}

func MakeCreateBookmarkEndpoint(s service.Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (response interface{}, err error) {
		req := request.(*CreateBookmarkRequest)

		err = s.CreateBookmark(
			ctx,
			domain.Bookmark{
				Name:        req.Name,
				Description: req.Description,
			},
		)

		if err != nil {
			return nil, err
		}

		return CreateBookmarkResponse{
			Result: "Created successfully",
		}, nil
	}
}

type ReadBookmarkRequest struct {
	Id int `json:"id"`
}

type ReadBookmarkResponse struct {
	Bookmark domain.Bookmark `json:"bookmark"`
}

func MakeReadBookmarkEndpoint(s service.Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (response interface{}, err error) {
		req := request.(*ReadBookmarkRequest)

		b, err := s.ReadBookmark(
			ctx,
			req.Id,
		)

		if err != nil {
			return nil, err
		}

		return ReadBookmarkResponse{
			Bookmark: b,
		}, nil
	}
}

type UpdateBookmarkRequest struct {
	// swagger:ignore
	BookmarkId  int    `json:"bookmark_id"`
	OwnerId     int    `json:"owner_id"`
	Name        string `json:"name"`
	Description string `json:"description"`
}

type UpdateBookmarkResponse struct {
	Bookmark domain.Bookmark `json:"bookmark"`
}

func MakeUpdateBookmarkEndpoint(s service.Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (response interface{}, err error) {
		req := request.(*UpdateBookmarkRequest)

		b, err := s.UpdateBookmark(
			ctx,
			domain.Bookmark{
				BookmarkId:  req.BookmarkId,
				OwnerId:     req.OwnerId,
				Name:        req.Name,
				Description: req.Description,
			},
		)

		if err != nil {
			return nil, err
		}

		return UpdateBookmarkResponse{
			Bookmark: b,
		}, nil
	}
}

type DeleteBookmarkRequest struct {
	Id int `json:"id"`
}

type DeleteBookmarkResponse struct {
	Result string `json:"result"`
}

func MakeDeleteBookmarkEndpoint(s service.Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (response interface{}, err error) {
		req := request.(*DeleteBookmarkRequest)

		err = s.DeleteBookmark(
			ctx,
			req.Id,
		)

		if err != nil {
			return nil, err
		}

		return DeleteBookmarkResponse{
			Result: "Deleted successfully",
		}, nil
	}
}

type ReadBookmarksRequest struct {
	Filter domain.BookmarkFilter `json:"filter"`
}

type ReadBookmarksResponse struct {
	Bookmarks []domain.Bookmark `json:"bookmarks"`
}

func MakeReadBookmarksEndpoint(s service.Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (response interface{}, err error) {
		req := request.(*ReadBookmarksRequest)

		b, err := s.ReadBookmarks(
			ctx,
			req.Filter,
		)

		if err != nil {
			return nil, err
		}

		return ReadBookmarksResponse{
			Bookmarks: b,
		}, nil
	}
}

package endpoints

import (
	"context"
	"errors"
	"github.com/Abunyawa/cinderella/domain"
	"github.com/Abunyawa/cinderella/service"
	"github.com/dgrijalva/jwt-go"
	"github.com/go-kit/kit/endpoint"
	log "github.com/sirupsen/logrus"
	"time"
)

type Endpoints struct {
	ExampleEndpoint      endpoint.Endpoint
	AddUserEndpoint      endpoint.Endpoint
	AuthUserEndpoint     endpoint.Endpoint
	RefreshTokenEndpoint endpoint.Endpoint
	CreateWorkspace      endpoint.Endpoint
	ReadWorkspace        endpoint.Endpoint
	UpdateWorkspace      endpoint.Endpoint
	DeleteWorkspace      endpoint.Endpoint
	ReadWorkspaces       endpoint.Endpoint
	CreateBookmark       endpoint.Endpoint
	ReadBookmark         endpoint.Endpoint
	UpdateBookmark       endpoint.Endpoint
	DeleteBookmark       endpoint.Endpoint
	ReadBookmarks        endpoint.Endpoint
	CreateTask           endpoint.Endpoint
	ReadTask             endpoint.Endpoint
	UpdateTask           endpoint.Endpoint
	DeleteTask           endpoint.Endpoint
	ReadTasks            endpoint.Endpoint
	CreateSubtask        endpoint.Endpoint
	ReadSubtask          endpoint.Endpoint
	UpdateSubtask        endpoint.Endpoint
	DeleteSubtask        endpoint.Endpoint
	ReadSubtasks         endpoint.Endpoint
}

func MakeEndpoints(s service.Service, sign_key string) Endpoints {
	return Endpoints{
		ExampleEndpoint:      MakeExampleEndpoint(s),
		AddUserEndpoint:      MakeAddUserEndpoint(s),
		AuthUserEndpoint:     MakeAuthUserEndpoint(s),
		RefreshTokenEndpoint: MakeRefreshTokenEndpoint(s),
		CreateWorkspace:      ParseToken(sign_key, MakeCreateWorkspaceEndpoint(s)),
		ReadWorkspace:        ParseToken(sign_key, MakeReadWorkspaceEndpoint(s)),
		UpdateWorkspace:      ParseToken(sign_key, MakeUpdateWorkspaceEndpoint(s)),
		DeleteWorkspace:      ParseToken(sign_key, MakeDeleteWorkspaceEndpoint(s)),
		ReadWorkspaces:       ParseToken(sign_key, MakeReadWorkspacesEndpoint(s)),
		CreateBookmark:       ParseToken(sign_key, MakeCreateBookmarkEndpoint(s)),
		ReadBookmark:         ParseToken(sign_key, MakeReadBookmarkEndpoint(s)),
		UpdateBookmark:       ParseToken(sign_key, MakeUpdateBookmarkEndpoint(s)),
		DeleteBookmark:       ParseToken(sign_key, MakeDeleteBookmarkEndpoint(s)),
		ReadBookmarks:        ParseToken(sign_key, MakeReadBookmarksEndpoint(s)),
		CreateTask:           ParseToken(sign_key, MakeCreateTaskEndpoint(s)),
		ReadTask:             ParseToken(sign_key, MakeReadTaskEndpoint(s)),
		UpdateTask:           ParseToken(sign_key, MakeUpdateTaskEndpoint(s)),
		DeleteTask:           ParseToken(sign_key, MakeDeleteTaskEndpoint(s)),
		ReadTasks:            ParseToken(sign_key, MakeReadTasksEndpoint(s)),
		CreateSubtask:        ParseToken(sign_key, MakeCreateSubtaskEndpoint(s)),
		ReadSubtask:          ParseToken(sign_key, MakeReadSubtaskEndpoint(s)),
		UpdateSubtask:        ParseToken(sign_key, MakeUpdateSubtaskEndpoint(s)),
		DeleteSubtask:        ParseToken(sign_key, MakeDeleteSubtaskEndpoint(s)),
		ReadSubtasks:         ParseToken(sign_key, MakeReadSubtasksEndpoint(s)),
	}
}

func ParseToken(sign_key string, next endpoint.Endpoint) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (response interface{}, err error) {
		tokenString, ok := ctx.Value(domain.JWTTokenContextKey).(string)
		if !ok {
			return nil, errors.New("missing token")
		}

		claims := &domain.Claims{}

		tkn, err := jwt.ParseWithClaims(tokenString, claims, func(token *jwt.Token) (interface{}, error) {
			return []byte(sign_key), nil
		})

		if err != nil {
			if err == jwt.ErrSignatureInvalid {
				return "", err
			}
			log.WithFields(log.Fields{
				"message":  err.Error(),
				"token":    tokenString,
				"sign_key": sign_key,
			}).Error("parsing error")
			return "", err
		}

		log.WithFields(log.Fields{}).Info("Token parsed")

		if !tkn.Valid {
			log.WithFields(log.Fields{
				"message": err.Error(),
			}).Error("invalid token")
			return "", errors.New("forbidden")
		}
		if time.Unix(claims.ExpiresAt, 0).Sub(time.Now()) < 0 {
			log.WithFields(log.Fields{
				"message": "token expired",
			}).Error("too old token")
			return "", errors.New("forbidden")
		}

		println(claims.Id)
		ctx = context.WithValue(ctx, domain.USER, claims.Id)
		println(ctx.Value(domain.USER).(int))
		return next(ctx, request)
	}
}

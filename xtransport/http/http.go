package xhttp

import (
	"context"
	"encoding/json"
	"github.com/Abunyawa/cinderella/domain"
	"github.com/go-kit/kit/endpoint"
	"net/http"
	"strings"

	"github.com/Abunyawa/cinderella/endpoints"
	"github.com/Abunyawa/cinderella/service"
	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"

	httptransport "github.com/go-kit/kit/transport/http"
)

func extractTokenFromAuthHeader(val string) (token string, ok bool) {
	authHeaderParts := strings.Split(val, " ")
	if len(authHeaderParts) != 2 || !strings.EqualFold(authHeaderParts[0], "bearer") {
		return "", false
	}

	return authHeaderParts[1], true
}

func HTTPToContext() httptransport.RequestFunc {
	return func(ctx context.Context, r *http.Request) context.Context {
		token, ok := extractTokenFromAuthHeader(r.Header.Get("Authorization"))
		if !ok {
			return ctx
		}
		ctx = context.WithValue(ctx, domain.JWTTokenContextKey, token)
		return ctx
	}
}

func MakeHTTPHandler(s service.Service, sign_key string) http.Handler {

	options := []httptransport.ServerOption{
		httptransport.ServerBefore(HTTPToContext()),
		httptransport.ServerErrorEncoder(errorEncoder),
	}

	r := mux.NewRouter()
	e := endpoints.MakeEndpoints(s, sign_key)

	example := httptransport.NewServer(
		e.ExampleEndpoint,
		decodeHTTPExampleRequest,
		encodeResponse,
		options...,
	)

	addUser := httptransport.NewServer(
		e.AddUserEndpoint,
		decodeHTTPAddUserRequest,
		encodeResponse,
		options...,
	)

	authUser := httptransport.NewServer(
		e.AuthUserEndpoint,
		decodeHTTPAuthUserRequest,
		encodeResponse,
		options...,
	)

	refreshToken := httptransport.NewServer(
		e.RefreshTokenEndpoint,
		decodeHTTPRefreshTokenRequest,
		encodeResponse,
		options...,
	)

	createWorkspace := httptransport.NewServer(
		e.CreateWorkspace,
		decodeHTTPCreateWorkspaceRequest,
		encodeResponse,
		options...,
	)

	readWorkspace := httptransport.NewServer(
		e.ReadWorkspace,
		decodeHTTPReadWorkspaceRequest,
		encodeResponse,
		options...,
	)

	updateWorkspace := httptransport.NewServer(
		e.UpdateWorkspace,
		decodeHTTPUpdateWorkspaceRequest,
		encodeResponse,
		options...,
	)

	deleteWorkspace := httptransport.NewServer(
		e.DeleteWorkspace,
		decodeHTTPDeleteWorkspaceRequest,
		encodeResponse,
		options...,
	)

	readWorkspaces := httptransport.NewServer(
		e.ReadWorkspaces,
		decodeHTTPReadWorkspacesRequest,
		encodeResponse,
		options...,
	)

	createBookmark := httptransport.NewServer(
		e.CreateBookmark,
		decodeHTTPCreateBookmarkRequest,
		encodeResponse,
		options...,
	)

	readBookmark := httptransport.NewServer(
		e.ReadBookmark,
		decodeHTTPReadBookmarkRequest,
		encodeResponse,
		options...,
	)

	updateBookmark := httptransport.NewServer(
		e.UpdateBookmark,
		decodeHTTPUpdateBookmarkRequest,
		encodeResponse,
		options...,
	)

	deleteBookmark := httptransport.NewServer(
		e.DeleteBookmark,
		decodeHTTPDeleteBookmarkRequest,
		encodeResponse,
		options...,
	)

	readBookmarks := httptransport.NewServer(
		e.ReadBookmarks,
		decodeHTTPReadBookmarksRequest,
		encodeResponse,
		options...,
	)

	createTask := httptransport.NewServer(
		e.CreateTask,
		decodeHTTPCreateTaskRequest,
		encodeResponse,
		options...,
	)

	readTask := httptransport.NewServer(
		e.ReadTask,
		decodeHTTPReadTaskRequest,
		encodeResponse,
		options...,
	)

	updateTask := httptransport.NewServer(
		e.UpdateTask,
		decodeHTTPUpdateTaskRequest,
		encodeResponse,
		options...,
	)

	deleteTask := httptransport.NewServer(
		e.DeleteTask,
		decodeHTTPDeleteTaskRequest,
		encodeResponse,
		options...,
	)

	readTasks := httptransport.NewServer(
		e.ReadTasks,
		decodeHTTPReadTasksRequest,
		encodeResponse,
		options...,
	)

	createSubtask := httptransport.NewServer(
		e.CreateSubtask,
		decodeHTTPCreateSubtaskRequest,
		encodeResponse,
		options...,
	)

	readSubtask := httptransport.NewServer(
		e.ReadSubtask,
		decodeHTTPReadSubtaskRequest,
		encodeResponse,
		options...,
	)

	updateSubtask := httptransport.NewServer(
		e.UpdateSubtask,
		decodeHTTPUpdateSubtaskRequest,
		encodeResponse,
		options...,
	)

	deleteSubtask := httptransport.NewServer(
		e.DeleteSubtask,
		decodeHTTPDeleteSubtaskRequest,
		encodeResponse,
		options...,
	)

	readSubtasks := httptransport.NewServer(
		e.ReadSubtasks,
		decodeHTTPReadSubtasksRequest,
		encodeResponse,
		options...,
	)

	swaggerRoutes(r, "/core")

	r.Handle("/core/example", example).Methods("POST")
	r.Handle("/core/register", addUser).Methods("POST")
	r.Handle("/core/auth", authUser).Methods("POST")
	r.Handle("/core/refresh", refreshToken).Methods("POST")
	r.Handle("/core/workspace", createWorkspace).Methods("POST")
	r.Handle("/core/workspaces", readWorkspaces).Methods("POST")
	r.Handle("/core/workspace/{id}", readWorkspace).Methods("GET")
	r.Handle("/core/workspace/{id}", updateWorkspace).Methods("PUT")
	r.Handle("/core/workspace/{id}", deleteWorkspace).Methods("DELETE")
	r.Handle("/core/bookmark", createBookmark).Methods("POST")
	r.Handle("/core/bookmarks", readBookmarks).Methods("POST")
	r.Handle("/core/bookmark/{id}", readBookmark).Methods("GET")
	r.Handle("/core/bookmark/{id}", updateBookmark).Methods("PUT")
	r.Handle("/core/bookmark/{id}", deleteBookmark).Methods("DELETE")
	r.Handle("/core/task", createTask).Methods("POST")
	r.Handle("/core/tasks", readTasks).Methods("POST")
	r.Handle("/core/task/{id}", readTask).Methods("GET")
	r.Handle("/core/task/{id}", updateTask).Methods("PUT")
	r.Handle("/core/task/{id}", deleteTask).Methods("DELETE")
	r.Handle("/core/subtask", createSubtask).Methods("POST")
	r.Handle("/core/subtasks", readSubtasks).Methods("POST")
	r.Handle("/core/subtask/{id}", readSubtask).Methods("GET")
	r.Handle("/core/subtask/{id}", updateSubtask).Methods("PUT")
	r.Handle("/core/subtask/{id}", deleteSubtask).Methods("DELETE")
	return handlers.CORS(handlers.AllowedMethods([]string{"*"}))(r)
}

type errorWrapper struct {
	Error string `json:"error"`
}

func errorEncoder(_ context.Context, err error, w http.ResponseWriter) {
	w.WriteHeader(err2code(err))
	json.NewEncoder(w).Encode(errorWrapper{Error: err.Error()})
}

func err2code(err error) int {
	switch err.Error() {
	case "forbidden":
		return http.StatusForbidden
	}
	return http.StatusInternalServerError
}

func encodeResponse(ctx context.Context, w http.ResponseWriter, response interface{}) error {
	if f, ok := response.(endpoint.Failer); ok && f.Failed() != nil {
		errorEncoder(ctx, f.Failed(), w)
		return nil
	}
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	return json.NewEncoder(w).Encode(response)
}

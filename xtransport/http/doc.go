// Cinderella API.
//
//	Schemes: https
//	Consumes:
//	- application/json
//
//	Produces:
//	- application/json
//
//	Security:
//	- api_key:
//
//	SecurityDefinitions:
//	api_key:
//	     type: apiKey
//	     name: authorization
//	     in: header
//
// swagger:meta
package xhttp

import (
	"encoding/json"
	"github.com/Abunyawa/cinderella/endpoints"
	"github.com/go-openapi/loads"
	"github.com/go-openapi/runtime/middleware"
	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"
)

func swaggerRoutes(r *mux.Router, basePath string) {
	//swagger serve -F=swagger ./swagger/swagger.json

	specDoc, err := loads.Spec("./swagger/swagger.json")
	if err == nil {
		specDoc.Spec().BasePath = "/"
		specDoc.Spec().Schemes = []string{"http"}

		b, err := json.MarshalIndent(specDoc.Spec(), "", "  ")
		if err == nil {
			opts := middleware.SwaggerUI(middleware.SwaggerUIOpts{SpecURL: "/swagger.json", BasePath: "/", Path: "/swagger-ui"}, nil)
			handler := handlers.CORS()(middleware.Spec("", b, opts))
			//swagger routes
			r.Handle("/swagger.json", middleware.Spec("", b, opts)).Methods("GET")
			r.Handle("/swagger-ui", handler).Methods("GET")

		}
	} else {
		log.WithFields(log.Fields{}).Error(err.Error())
	}
}

//swagger:parameters swaggerRegisterRequest
type _ struct {
	//in: body
	Body endpoints.AddUserRequest
}

//swagger:response swaggerRegisterResponse
type _ struct {
	//in: body
	Body endpoints.AddUserResponse
}

//swagger:route POST /core/register Auth swaggerRegisterRequest
// responses:
//	 200:swaggerRegisterResponse

//swagger:parameters swaggerAuthRequest
type _ struct {
	//in: body
	Body endpoints.AuthUserRequest
}

//swagger:response swaggerAuthResponse
type _ struct {
	//in: body
	Body endpoints.AuthUserResponse
}

//swagger:route POST /core/auth Auth swaggerAuthRequest
// responses:
//	 200:swaggerAuthResponse

//swagger:parameters swaggerRefreshTokenRequest
type _ struct {
	//in: body
	Body endpoints.RefreshTokenRequest
}

//swagger:response swaggerRefreshTokenResponse
type _ struct {
	//in: body
	Body endpoints.RefreshTokenResponse
}

//swagger:route POST /core/refresh Auth swaggerRefreshTokenRequest
// responses:
//	 200:swaggerRefreshTokenResponse

//swagger:parameters swaggerCreateWorkspaceRequest
type _ struct {
	//in: body
	Body endpoints.CreateWorkspaceRequest
}

//swagger:response swaggerCreateWorkspaceResponse
type _ struct {
	//in: body
	Body endpoints.CreateWorkspaceResponse
}

//swagger:route POST /core/workspace Workspace swaggerCreateWorkspaceRequest
// responses:
//	 200:swaggerCreateWorkspaceResponse

//swagger:parameters swaggerReadWorkspacesRequest
type _ struct {
	//in: body
	Body endpoints.ReadWorkspacesRequest
}

//swagger:response swaggerReadWorkspacesResponse
type _ struct {
	//in: body
	Body endpoints.ReadWorkspacesResponse
}

//swagger:route POST /core/workspaces Workspace swaggerReadWorkspacesRequest
// responses:
//	 200:swaggerReadWorkspacesResponse

//swagger:parameters swaggerReadWorkspaceRequest
type _ struct {
	//in: path
	Id int `json:"id"`
}

//swagger:response swaggerReadWorkspaceResponse
type _ struct {
	//in: body
	Body endpoints.ReadWorkspaceResponse
}

//swagger:route GET /core/workspace/{id} Workspace swaggerReadWorkspaceRequest
// responses:
//	 200:swaggerReadWorkspaceResponse

//swagger:parameters swaggerUpdateWorkspaceRequest
type _ struct {
	//in: path
	Id int `json:"id"`
	//in: body
	Body endpoints.UpdateWorkspaceRequest
}

//swagger:response swaggerUpdateWorkspaceResponse
type _ struct {
	//in: body
	Body endpoints.UpdateWorkspaceResponse
}

//swagger:route PUT /core/workspace/{id} Workspace swaggerUpdateWorkspaceRequest
// responses:
//	 200:swaggerUpdateWorkspaceResponse

//swagger:parameters swaggerDeleteWorkspaceRequest
type _ struct {
	//in: path
	Id int `json:"id"`
}

//swagger:response swaggerDeleteWorkspaceResponse
type _ struct {
	//in: body
	Body endpoints.DeleteWorkspaceResponse
}

//swagger:route DELETE /core/workspace/{id} Workspace swaggerDeleteWorkspaceRequest
// responses:
//	 200:swaggerDeleteWorkspaceResponse

//swagger:parameters swaggerCreateBookmarkRequest
type _ struct {
	//in: body
	Body endpoints.CreateBookmarkRequest
}

//swagger:response swaggerCreateBookmarkResponse
type _ struct {
	//in: body
	Body endpoints.CreateBookmarkResponse
}

//swagger:route POST /core/bookmark Bookmark swaggerCreateBookmarkRequest
// responses:
//	 200:swaggerCreateBookmarkResponse

//swagger:parameters swaggerReadBookmarksRequest
type _ struct {
	//in: body
	Body endpoints.ReadBookmarksRequest
}

//swagger:response swaggerReadBookmarksResponse
type _ struct {
	//in: body
	Body endpoints.ReadBookmarksResponse
}

//swagger:route POST /core/bookmarks Bookmark swaggerReadBookmarksRequest
// responses:
//	 200:swaggerReadBookmarksResponse

//swagger:parameters swaggerReadBookmarkRequest
type _ struct {
	//in: path
	Id int `json:"id"`
}

//swagger:response swaggerReadBookmarkResponse
type _ struct {
	//in: body
	Body endpoints.ReadBookmarkResponse
}

//swagger:route GET /core/bookmark/{id} Bookmark swaggerReadBookmarkRequest
// responses:
//	 200:swaggerReadBookmarkResponse

//swagger:parameters swaggerUpdateBookmarkRequest
type _ struct {
	//in: path
	Id int `json:"id"`
	//in: body
	Body endpoints.UpdateBookmarkRequest
}

//swagger:response swaggerUpdateBookmarkResponse
type _ struct {
	//in: body
	Body endpoints.UpdateBookmarkResponse
}

//swagger:route PUT /core/bookmark/{id} Bookmark swaggerUpdateBookmarkRequest
// responses:
//	 200:swaggerUpdateBookmarkResponse

//swagger:parameters swaggerDeleteBookmarkRequest
type _ struct {
	//in: path
	Id int `json:"id"`
}

//swagger:response swaggerDeleteBookmarkResponse
type _ struct {
	//in: body
	Body endpoints.DeleteBookmarkResponse
}

//swagger:route DELETE /core/bookmark/{id} Bookmark swaggerDeleteBookmarkRequest
// responses:
//	 200:swaggerDeleteBookmarkResponse

//swagger:parameters swaggerCreateTaskRequest
type _ struct {
	//in: body
	Body endpoints.CreateTaskRequest
}

//swagger:response swaggerCreateTaskResponse
type _ struct {
	//in: body
	Body endpoints.CreateTaskResponse
}

//swagger:route POST /core/task Task swaggerCreateTaskRequest
// responses:
//	 200:swaggerCreateTaskResponse

//swagger:parameters swaggerReadTasksRequest
type _ struct {
	//in: body
	Body endpoints.ReadTasksRequest
}

//swagger:response swaggerReadTasksResponse
type _ struct {
	//in: body
	Body endpoints.ReadTasksResponse
}

//swagger:route POST /core/tasks Task swaggerReadTasksRequest
// responses:
//	 200:swaggerReadTasksResponse

//swagger:parameters swaggerReadTaskRequest
type _ struct {
	//in: path
	Id int `json:"id"`
}

//swagger:response swaggerReadTaskResponse
type _ struct {
	//in: body
	Body endpoints.ReadTaskResponse
}

//swagger:route GET /core/task/{id} Task swaggerReadTaskRequest
// responses:
//	 200:swaggerReadTaskResponse

//swagger:parameters swaggerUpdateTaskRequest
type _ struct {
	//in: path
	Id int `json:"id"`
	//in: body
	Body endpoints.UpdateTaskRequest
}

//swagger:response swaggerUpdateTaskResponse
type _ struct {
	//in: body
	Body endpoints.UpdateTaskResponse
}

//swagger:route PUT /core/task/{id} Task swaggerUpdateTaskRequest
// responses:
//	 200:swaggerUpdateTaskResponse

//swagger:parameters swaggerDeleteTaskRequest
type _ struct {
	//in: path
	Id int `json:"id"`
}

//swagger:response swaggerDeleteTaskResponse
type _ struct {
	//in: body
	Body endpoints.DeleteTaskResponse
}

//swagger:route DELETE /core/task/{id} Task swaggerDeleteTaskRequest
// responses:
//	 200:swaggerDeleteTaskResponse

//swagger:parameters swaggerCreateSubtaskRequest
type _ struct {
	//in: body
	Body endpoints.CreateSubtaskRequest
}

//swagger:response swaggerCreateSubtaskResponse
type _ struct {
	//in: body
	Body endpoints.CreateSubtaskResponse
}

//swagger:route POST /core/subtask Subtask swaggerCreateSubtaskRequest
// responses:
//	 200:swaggerCreateSubtaskResponse

//swagger:parameters swaggerReadSubtasksRequest
type _ struct {
	//in: body
	Body endpoints.ReadSubtasksRequest
}

//swagger:response swaggerReadSubtasksResponse
type _ struct {
	//in: body
	Body endpoints.ReadSubtasksResponse
}

//swagger:route POST /core/subtasks Subtask swaggerReadSubtasksRequest
// responses:
//	 200:swaggerReadSubtasksResponse

//swagger:parameters swaggerReadSubtaskRequest
type _ struct {
	//in: path
	Id int `json:"id"`
}

//swagger:response swaggerReadSubtaskResponse
type _ struct {
	//in: body
	Body endpoints.ReadSubtaskResponse
}

//swagger:route GET /core/subtask/{id} Subtask swaggerReadSubtaskRequest
// responses:
//	 200:swaggerReadSubtaskResponse

//swagger:parameters swaggerUpdateSubtaskRequest
type _ struct {
	//in: path
	Id int `json:"id"`
	//in: body
	Body endpoints.UpdateSubtaskRequest
}

//swagger:response swaggerUpdateSubtaskResponse
type _ struct {
	//in: body
	Body endpoints.UpdateSubtaskResponse
}

//swagger:route PUT /core/subtask/{id} Subtask swaggerUpdateSubtaskRequest
// responses:
//	 200:swaggerUpdateSubtaskResponse

//swagger:parameters swaggerDeleteSubtaskRequest
type _ struct {
	//in: path
	Id int `json:"id"`
}

//swagger:response swaggerDeleteSubtaskResponse
type _ struct {
	//in: body
	Body endpoints.DeleteSubtaskResponse
}

//swagger:route DELETE /core/subtask/{id} Subtask swaggerDeleteSubtaskRequest
// responses:
//	 200:swaggerDeleteSubtaskResponse

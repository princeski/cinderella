package xhttp

import (
	"context"
	"encoding/json"
	"errors"
	"github.com/Abunyawa/cinderella/endpoints"
	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"
	"net/http"
	"strconv"
)

func decodeHTTPCreateTaskRequest(_ context.Context, r *http.Request) (request interface{}, err error) {
	req := &endpoints.CreateTaskRequest{}

	if err := json.NewDecoder(r.Body).Decode(req); err != nil {
		log.WithFields(log.Fields{}).Error("error reading request body")
		return nil, err
	}

	log.WithFields(log.Fields{}).Info("Got request")
	return req, nil
}

func decodeHTTPReadTaskRequest(_ context.Context, r *http.Request) (request interface{}, err error) {
	req := &endpoints.ReadTaskRequest{}

	vars := mux.Vars(r)
	id, ok := vars["id"]
	if !ok {
		return nil, errors.New("id not provided")
	}

	intId, err := strconv.Atoi(id)
	if err != nil {
		return nil, errors.New("id invalid")
	}
	req.Id = intId

	log.WithFields(log.Fields{}).Info("Got request")
	return req, nil
}

func decodeHTTPUpdateTaskRequest(_ context.Context, r *http.Request) (request interface{}, err error) {
	req := &endpoints.UpdateTaskRequest{}

	if err := json.NewDecoder(r.Body).Decode(req); err != nil {
		log.WithFields(log.Fields{}).Error("error reading request body")
		return nil, err
	}

	vars := mux.Vars(r)
	id, ok := vars["id"]
	if !ok {
		return nil, errors.New("id not provided")
	}

	intId, err := strconv.Atoi(id)
	if err != nil {
		return nil, errors.New("id invalid")
	}
	req.TaskId = intId

	log.WithFields(log.Fields{}).Info("Got request")
	return req, nil
}

func decodeHTTPDeleteTaskRequest(_ context.Context, r *http.Request) (request interface{}, err error) {
	req := &endpoints.DeleteTaskRequest{}

	vars := mux.Vars(r)
	id, ok := vars["id"]
	if !ok {
		return nil, errors.New("id not provided")
	}

	intId, err := strconv.Atoi(id)
	if err != nil {
		return nil, errors.New("id invalid")
	}
	req.Id = intId

	log.WithFields(log.Fields{}).Info("Got request")
	return req, nil
}

func decodeHTTPReadTasksRequest(_ context.Context, r *http.Request) (request interface{}, err error) {
	req := &endpoints.ReadTasksRequest{}

	if err := json.NewDecoder(r.Body).Decode(req); err != nil {
		log.WithFields(log.Fields{}).Error("error reading request body")
		return nil, err
	}

	if req.Filter.Size == 0 {
		req.Filter.Size = 20
	}

	log.WithFields(log.Fields{}).Info("Got request")
	return req, nil
}

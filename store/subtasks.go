package store

import (
	"fmt"
	"github.com/Abunyawa/cinderella/domain"
	log "github.com/sirupsen/logrus"
)

func (s *store) CreateSubtask(st domain.Subtask) error {
	query := "INSERT INTO subtasks (task_id, owner_id, name, description, status) VALUES ($1, $2, $3, $4)"

	if _, err := s.db.Exec(query, st.TaskId, st.OwnerId, st.Name, st.Description, st.Status); err != nil {
		log.WithFields(log.Fields{
			"method":  domain.CREATE,
			"message": err.Error(),
		}).Error("database error")
		return err
	}

	log.WithFields(log.Fields{
		"method": domain.CREATE,
		"id":     st.SubtaskId,
	}).Info("Database success")

	return nil
}

func (s *store) ReadSubtask(id int) (domain.Subtask, error) {
	query := "SELECT substask_id, task_id, owner_id, created_at, name, description, status FROM subtasks WHERE subtask_id = $1"
	st := domain.Subtask{}
	st.SubtaskId = id
	if err := s.db.QueryRow(query, id).Scan(&st.SubtaskId, &st.TaskId, &st.OwnerId, &st.CreatedAt, &st.Name, &st.Description, &st.Status); err != nil {
		log.WithFields(log.Fields{
			"method":  domain.READ,
			"message": err.Error(),
		}).Error("database error")
		return st, err
	}

	log.WithFields(log.Fields{
		"method": domain.READ,
		"id":     st.SubtaskId,
	}).Info("Database success")

	return st, nil
}

func (s *store) UpdateSubtask(st domain.Subtask) (domain.Subtask, error) {
	query := "UPDATE subtasks SET " +
		"task_id = $1, " +
		"owner_id = $2, " +
		"created_at = $3, " +
		"name = $4, " +
		"description = $5, " +
		"status = $6 " +
		"WHERE subtask_id = $7"

	if _, err := s.db.Exec(query, st.TaskId, st.OwnerId, st.CreatedAt, st.Name, st.Description, st.Status, st.SubtaskId); err != nil {
		log.WithFields(log.Fields{
			"method":  domain.UPDATE,
			"message": err.Error(),
		}).Error("database error")
		return st, err
	}

	log.WithFields(log.Fields{
		"method": domain.UPDATE,
		"id":     st.SubtaskId,
	}).Info("Database success")

	return st, nil
}

func (s *store) DeleteSubtask(id, owner_id int) error {
	query := "DELETE FROM subtasks WHERE subtask_id = $1 AND owner_id = $2"

	if _, err := s.db.Exec(query, id, owner_id); err != nil {
		log.WithFields(log.Fields{
			"method":  domain.DELETE,
			"id":      id,
			"message": err.Error(),
		}).Error("database error")
		return err
	}

	log.WithFields(log.Fields{
		"method": domain.DELETE,
		"id":     id,
	}).Info("Database success")

	return nil
}

func (s *store) ReadSubtasks(filter domain.SubtaskFilter, ownerId int) ([]domain.Subtask, error) {
	vals := []interface{}{}
	query := "SELECT substask_id, task_id, owner_id, created_at, name, description, status FROM subtasks WHERE owner_id = $1 "
	vals = append(vals, ownerId)
	k := 2

	if filter.TaskId != 0 {
		query += fmt.Sprintf("AND task_id = $%d ", k)
		vals = append(vals, filter.TaskId)
		k++
	}

	if filter.CreatedAtFrom != nil {
		query += fmt.Sprintf("AND created_at >= $%d ", k)
		vals = append(vals, filter.CreatedAtFrom)
		k++
	}

	if filter.CreatedAtTo != nil {
		query += fmt.Sprintf("AND created_at <= $%d ", k)
		vals = append(vals, filter.CreatedAtTo)
		k++
	}

	if filter.Name != "" {
		query += fmt.Sprintf("AND name LIKE $%d ", k)
		vals = append(vals, "%"+filter.Name+"%")
		k++
	}

	if filter.Description != "" {
		query += fmt.Sprintf("AND description LIKE $%d ", k)
		vals = append(vals, "%"+filter.Description+"%")
		k++
	}

	if filter.Status != "" {
		query += fmt.Sprintf("AND status = $%d ", k)
		vals = append(vals, filter.Status)
		k++
	}

	query += fmt.Sprintf("LIMIT $%d OFFSET $%d", k, k+1)
	vals = append(vals, filter.Size, filter.Size*filter.Page)

	ret := []domain.Subtask{}
	rows, err := s.db.Query(query, vals...)
	if err != nil {
		log.WithFields(log.Fields{
			"method":  domain.READ,
			"message": err.Error(),
		}).Error("database error")
		return ret, err
	}

	for rows.Next() {
		st := domain.Subtask{}
		if err := rows.Scan(&st.SubtaskId, &st.TaskId, &st.OwnerId, &st.CreatedAt, &st.Name, &st.Description, &st.Status); err != nil {
			log.WithFields(log.Fields{
				"method":  domain.READ,
				"message": err.Error(),
			}).Error("database error")
			return ret, err
		}
		ret = append(ret, st)
	}

	log.WithFields(log.Fields{
		"method": domain.READ,
	}).Info("Database success")

	return ret, nil
}

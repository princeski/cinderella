package store

import (
	"fmt"
	"github.com/Abunyawa/cinderella/domain"
	log "github.com/sirupsen/logrus"
)

func (s *store) CreateBookmark(b domain.Bookmark) error {
	query := "INSERT INTO bookmarks (owner_id, name, description) VALUES ($1, $2, $3)"

	if _, err := s.db.Exec(query, b.OwnerId, b.Name, b.Description); err != nil {
		log.WithFields(log.Fields{
			"method":  domain.CREATE,
			"id":      b.OwnerId,
			"message": err.Error(),
		}).Error("database error")
		return err
	}

	log.WithFields(log.Fields{
		"method": domain.CREATE,
		"id":     b.OwnerId,
	}).Info("Database success")

	return nil
}

func (s *store) ReadBookmark(id int) (domain.Bookmark, error) {
	query := "SELECT bookmark_id, owner_id, name, description FROM bookmarks WHERE bookmark_id = $1"
	b := domain.Bookmark{}
	b.BookmarkId = id
	if err := s.db.QueryRow(query, id).Scan(&b.BookmarkId, &b.OwnerId, &b.Name, &b.Description); err != nil {
		log.WithFields(log.Fields{
			"method":  domain.READ,
			"message": err.Error(),
		}).Error("database error")
		return b, err
	}

	log.WithFields(log.Fields{
		"method": domain.READ,
		"id":     b.OwnerId,
	}).Info("Database success")

	return b, nil
}

func (s *store) UpdateBookmark(b domain.Bookmark) (domain.Bookmark, error) {
	query := "UPDATE bookmarks SET owner_id = $1, name = $2, description = $3 WHERE bookmark_id = $4"

	if _, err := s.db.Exec(query, b.OwnerId, b.Name, b.Description, b.BookmarkId); err != nil {
		log.WithFields(log.Fields{
			"method":  domain.UPDATE,
			"message": err.Error(),
		}).Error("database error")
		return b, err
	}

	log.WithFields(log.Fields{
		"method": domain.UPDATE,
		"id":     b.OwnerId,
	}).Info("Database success")

	return b, nil
}

func (s *store) DeleteBookmark(id, owner_id int) error {
	query := "DELETE FROM bookmarks WHERE bookmark_id = $1 AND owner_id = $2"

	if _, err := s.db.Exec(query, id, owner_id); err != nil {
		log.WithFields(log.Fields{
			"method":  domain.DELETE,
			"id":      id,
			"message": err.Error(),
		}).Error("database error")
		return err
	}

	log.WithFields(log.Fields{
		"method": domain.DELETE,
		"id":     id,
	}).Info("Database success")

	return nil
}

func (s *store) ReadBookmarks(filter domain.BookmarkFilter, ownerId int) ([]domain.Bookmark, error) {
	vals := []interface{}{}
	query := "SELECT bookmark_id, owner_id, name, description FROM bookmarks WHERE owner_id = $1 "
	vals = append(vals, ownerId)
	k := 2

	if filter.Name != "" {
		query += fmt.Sprintf("AND name LIKE $%d ", k)
		vals = append(vals, "%"+filter.Name+"%")
		k++
	}

	if filter.Description != "" {
		query += fmt.Sprintf("AND description LIKE $%d ", k)
		vals = append(vals, "%"+filter.Description+"%")
		k++
	}

	query += fmt.Sprintf("LIMIT $%d OFFSET $%d", k, k+1)
	vals = append(vals, filter.Size, filter.Size*filter.Page)

	ret := []domain.Bookmark{}
	rows, err := s.db.Query(query, vals...)
	if err != nil {
		log.WithFields(log.Fields{
			"method":  domain.READ,
			"message": err.Error(),
		}).Error("database error")
		return ret, err
	}

	for rows.Next() {
		b := domain.Bookmark{}
		if err := rows.Scan(&b.BookmarkId, &b.OwnerId, &b.Name, &b.Description); err != nil {
			log.WithFields(log.Fields{
				"method":  domain.READ,
				"message": err.Error(),
			}).Error("database error")
			return ret, err
		}
		ret = append(ret, b)
	}

	log.WithFields(log.Fields{
		"method": domain.READ,
	}).Info("Database success")

	return ret, nil
}

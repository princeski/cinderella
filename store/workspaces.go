package store

import (
	"github.com/Abunyawa/cinderella/domain"
	log "github.com/sirupsen/logrus"
)

func (s *store) CreateWorkspace(ws domain.Workspace) error {
	query := "INSERT INTO workspaces (owner_id, name) VALUES ($1, $2)"

	if _, err := s.db.Exec(query, ws.OwnerId, ws.Name); err != nil {
		log.WithFields(log.Fields{
			"method":  domain.CREATE,
			"id":      ws.OwnerId,
			"message": err.Error(),
		}).Error("database error")
		return err
	}

	log.WithFields(log.Fields{
		"method": domain.CREATE,
		"id":     ws.OwnerId,
	}).Info("Database success")

	return nil
}

func (s *store) ReadWorkspace(id int) (domain.Workspace, error) {
	query := "SELECT workspace_id, owner_id, name FROM workspaces WHERE workspace_id = $1"
	ws := domain.Workspace{}
	ws.WorkspaceId = id
	if err := s.db.QueryRow(query, id).Scan(&ws.WorkspaceId, &ws.OwnerId, &ws.Name); err != nil {
		log.WithFields(log.Fields{
			"method":  domain.READ,
			"id":      ws.OwnerId,
			"message": err.Error(),
		}).Error("database error")
		return ws, err
	}

	log.WithFields(log.Fields{
		"method": domain.READ,
		"id":     ws.OwnerId,
	}).Info("Database success")

	return ws, nil
}

func (s *store) UpdateWorkspace(ws domain.Workspace) (domain.Workspace, error) {
	query := "UPDATE workspaces SET owner_id = $1, name = $2 WHERE workspace_id = $3"

	if _, err := s.db.Exec(query, ws.OwnerId, ws.Name, ws.WorkspaceId); err != nil {
		log.WithFields(log.Fields{
			"method":  domain.UPDATE,
			"id":      ws.OwnerId,
			"message": err.Error(),
		}).Error("database error")
		return ws, err
	}

	log.WithFields(log.Fields{
		"method": domain.UPDATE,
		"id":     ws.OwnerId,
	}).Info("Database success")

	return ws, nil
}

func (s *store) DeleteWorkspace(id, owner_id int) error {
	query := "DELETE FROM workspaces WHERE workspace_id = $1 AND owner_id = $2"

	if _, err := s.db.Exec(query, id, owner_id); err != nil {
		log.WithFields(log.Fields{
			"method":  domain.DELETE,
			"id":      id,
			"message": err.Error(),
		}).Error("database error")
		return err
	}

	log.WithFields(log.Fields{
		"method": domain.DELETE,
		"id":     id,
	}).Info("Database success")

	return nil
}

func (s *store) ReadWorkspaces(filter domain.WorkspaceFilter, ownerId int) ([]domain.Workspace, error) {
	query := "SELECT workspace_id, owner_id, name FROM workspaces WHERE owner_id = $1 LIMIT $2 OFFSET $3"
	ret := []domain.Workspace{}
	rows, err := s.db.Query(query, ownerId, filter.Size, filter.Page*filter.Size)
	if err != nil {
		log.WithFields(log.Fields{
			"method":  domain.READ,
			"message": err.Error(),
		}).Error("database error")
		return ret, err
	}

	for rows.Next() {
		ws := domain.Workspace{}
		if err := rows.Scan(&ws.WorkspaceId, &ws.OwnerId, &ws.Name); err != nil {
			log.WithFields(log.Fields{
				"method":  domain.READ,
				"message": err.Error(),
			}).Error("database error")
			return ret, err
		}
		ret = append(ret, ws)
	}

	log.WithFields(log.Fields{
		"method": domain.READ,
	}).Info("Database success")

	return ret, nil
}

package store

import (
	"fmt"
	"github.com/Abunyawa/cinderella/domain"
	log "github.com/sirupsen/logrus"
)

func (s *store) CreateTask(t domain.Task) error {
	query := "INSERT INTO tasks (workspace_id, owner_id, status, name, description, deadline) VALUES ($1, $2, $3)"

	if _, err := s.db.Exec(query, t.WorkspaceId, t.OwnerId, t.Status, t.Name, t.Description, t.Deadline); err != nil {
		log.WithFields(log.Fields{
			"method":  domain.CREATE,
			"message": err.Error(),
		}).Error("database error")
		return err
	}

	log.WithFields(log.Fields{
		"method": domain.CREATE,
		"id":     t.OwnerId,
	}).Info("Database success")

	return nil
}

func (s *store) ReadTask(id int) (domain.Task, error) {
	query := "SELECT task_id, workspace_id, owner_id, status, created_at, name, description, deadline FROM tasks WHERE task_id = $1"
	t := domain.Task{}
	t.TaskId = id
	if err := s.db.QueryRow(query, id).Scan(
		&t.TaskId,
		&t.WorkspaceId,
		&t.OwnerId,
		&t.Status,
		&t.CreatedAt,
		&t.Name,
		&t.Description,
		&t.Deadline,
	); err != nil {
		log.WithFields(log.Fields{
			"method":  domain.READ,
			"message": err.Error(),
		}).Error("database error")
		return t, err
	}

	log.WithFields(log.Fields{
		"method": domain.READ,
		"id":     t.OwnerId,
	}).Info("Database success")

	return t, nil
}

func (s *store) UpdateTask(t domain.Task) (domain.Task, error) {
	query := "UPDATE tasks SET workspace_id = $1, " +
		"owner_id = $2, " +
		"status = $3, " +
		"created_at = $4, " +
		"name = $5, " +
		"description = $6, " +
		"deadline = $7 " +
		"WHERE task_id = $8"

	if _, err := s.db.Exec(query,
		t.WorkspaceId,
		t.OwnerId,
		t.Status,
		t.CreatedAt,
		t.Name,
		t.Description,
		t.Deadline,
		t.TaskId,
	); err != nil {
		log.WithFields(log.Fields{
			"method":  domain.UPDATE,
			"message": err.Error(),
		}).Error("database error")
		return t, err
	}

	log.WithFields(log.Fields{
		"method": domain.UPDATE,
		"id":     t.OwnerId,
	}).Info("Database success")

	return t, nil
}

func (s *store) DeleteTask(id, owner_id int) error {
	query := "DELETE FROM tasks WHERE task_id = $1 AND owner_id = $2"

	if _, err := s.db.Exec(query, id, owner_id); err != nil {
		log.WithFields(log.Fields{
			"method":  domain.DELETE,
			"id":      id,
			"message": err.Error(),
		}).Error("database error")
		return err
	}

	log.WithFields(log.Fields{
		"method": domain.DELETE,
		"id":     id,
	}).Info("Database success")

	return nil
}

func (s *store) ReadTasks(filter domain.TaskFilter, ownerId int) ([]domain.Task, error) {
	vals := []interface{}{}
	query := "SELECT task_id, workspace_id, owner_id, status, created_at, name, description, deadline FROM tasks WHERE owner_id = $1 "
	vals = append(vals, ownerId)
	k := 2

	if filter.WorkspaceId != 0 {
		query += fmt.Sprintf("AND workspace_id = $%d ", k)
		vals = append(vals, filter.WorkspaceId)
		k++
	}

	if filter.Status != "" {
		query += fmt.Sprintf("AND status = $%d ", k)
		vals = append(vals, filter.Status)
		k++
	}

	if filter.CreatedAtFrom != nil {
		query += fmt.Sprintf("AND created_at >= $%d ", k)
		vals = append(vals, filter.CreatedAtFrom)
		k++
	}

	if filter.CreatedAtTo != nil {
		query += fmt.Sprintf("AND created_at <= $%d ", k)
		vals = append(vals, filter.CreatedAtTo)
		k++
	}

	if filter.Name != "" {
		query += fmt.Sprintf("AND name LIKE $%d ", k)
		vals = append(vals, "%"+filter.Name+"%")
		k++
	}

	if filter.Description != "" {
		query += fmt.Sprintf("AND description LIKE $%d ", k)
		vals = append(vals, "%"+filter.Description+"%")
		k++
	}

	if filter.DeadlineFrom != nil {
		query += fmt.Sprintf("AND deadline >= $%d ", k)
		vals = append(vals, filter.DeadlineFrom)
		k++
	}

	if filter.DeadlineTo != nil {
		query += fmt.Sprintf("AND deadline <= $%d ", k)
		vals = append(vals, filter.DeadlineTo)
		k++
	}

	query += fmt.Sprintf("LIMIT $%d OFFSET $%d", k, k+1)
	vals = append(vals, filter.Size, filter.Size*filter.Page)

	ret := []domain.Task{}
	rows, err := s.db.Query(query, vals...)
	if err != nil {
		log.WithFields(log.Fields{
			"method":  domain.READ,
			"message": err.Error(),
		}).Error("database error")
		return ret, err
	}

	for rows.Next() {
		t := domain.Task{}
		if err := rows.Scan(
			&t.TaskId,
			&t.WorkspaceId,
			&t.OwnerId,
			&t.Status,
			&t.CreatedAt,
			&t.Name,
			&t.Description,
			&t.Deadline,
		); err != nil {
			log.WithFields(log.Fields{
				"method":  domain.READ,
				"message": err.Error(),
			}).Error("database error")
			return ret, err
		}
		ret = append(ret, t)
	}

	log.WithFields(log.Fields{
		"method": domain.READ,
	}).Info("Database success")

	return ret, nil
}

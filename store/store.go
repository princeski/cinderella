package store

import (
	"database/sql"

	"github.com/Abunyawa/cinderella/domain"
)

type Store interface {
	AddUser(user *domain.User) error
	VerifyUser(user *domain.User) (*domain.User, error)

	CreateWorkspace(ws domain.Workspace) error
	ReadWorkspace(id int) (domain.Workspace, error)
	UpdateWorkspace(ws domain.Workspace) (domain.Workspace, error)
	DeleteWorkspace(id, owner_id int) error
	ReadWorkspaces(filter domain.WorkspaceFilter, ownerId int) ([]domain.Workspace, error)

	CreateBookmark(b domain.Bookmark) error
	ReadBookmark(id int) (domain.Bookmark, error)
	UpdateBookmark(b domain.Bookmark) (domain.Bookmark, error)
	DeleteBookmark(id, owner_id int) error
	ReadBookmarks(filter domain.BookmarkFilter, ownerId int) ([]domain.Bookmark, error)

	CreateTask(t domain.Task) error
	ReadTask(id int) (domain.Task, error)
	UpdateTask(t domain.Task) (domain.Task, error)
	DeleteTask(id, owner_id int) error
	ReadTasks(filter domain.TaskFilter, ownerId int) ([]domain.Task, error)

	CreateSubtask(st domain.Subtask) error
	ReadSubtask(id int) (domain.Subtask, error)
	UpdateSubtask(st domain.Subtask) (domain.Subtask, error)
	DeleteSubtask(id, owner_id int) error
	ReadSubtasks(filter domain.SubtaskFilter, ownerId int) ([]domain.Subtask, error)
}
type store struct {
	db *sql.DB
}

func NewStore(db *sql.DB) Store {
	return &store{db: db}
}
